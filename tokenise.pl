#!/usr/bin/perl -w

# Entrée: un corpus normalisé (peut contenir du XML)
# Sortie: un token par ligne
#           (si nbTokens=1 : nb de tokens, suivi d'une tabulation, suivi de la liste des tokens séparés par des tabulations)

use strict; 
use warnings;

use File::Slurp;
use Switch;
use Getopt::Long;

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Lecture des arguments
my $config_verbose = 0;
my $config_fileDico = 0;
my $config_window = 5;  # Fenêtre de tokens par défaut à considérer si on utilise un dictionnaire pour fusionner les tokens (option -l)
my $config_nbTokens = 0;  # Afficher le nb de tokens, suivi d'une tabulation, suivi de la liste des tokens séparés par des tabulations
my $config_unk = 0;
my $config_ocr = '';
my $config_pass1 = '';
my $config_fallbackRmhyphens = 0;  # Si token non trouvé, regarder si il s'agit d'un mot contenant un tiret de fin de ligne (hyphen). Req pass1.
my $config_fallbackSuperseg = '';  # Si token non trouvé, regarder si on peut le sur-segmenter. Rq un argument (fichier: liste des préfixes). Req pass1.
my $config_fallbackSupersegSufix = '';  # Si token non trouvé, regarder si on peut le sur-segmenter. Rq un argument (fichier: liste des sufixes). Req pass1.
my $config_fallbackSupersegNP = 0;  # Si token non trouvé, regarder si on peut le sur-segmenter et trouver un nom propre. Req pass1.
my $config_fallbackMergeseg = 0;  # Si token non trouvé, regarder si on peut le fusionner avec ses voisins. Req pass1 et fallbackRmhyphens.
GetOptions(
  'lexicon|l=s'=>\$config_fileDico,
  'verbose|v'=>\$config_verbose,
  'window|w=s'=>\$config_window,
  'nbTokens'=>\$config_nbTokens,
  'unk'=>\$config_unk,
  'ocr=s'=>\$config_ocr,
  'pass1=s'=>\$config_pass1,
  'fallbackRmhyphens'=>\$config_fallbackRmhyphens,
  'fallbackSuperseg|p=s'=>\$config_fallbackSuperseg,
  'fallbackSupersegNP'=>\$config_fallbackSupersegNP,
  'fallbackSupersegSufix|s=s'=>\$config_fallbackSupersegSufix,
  'fallbackMergeseg'=>\$config_fallbackMergeseg
);

# Chargement du lexique

if(! $config_fileDico) {
  print STDERR "ATTENTION: tokenisation sans dictionnaire. Seule la tokenisation max sera effectuée. \n";
}

if($config_fileDico && ! -e $config_fileDico) {
  print STDERR "ERREUR: impossible de charger le dictionnaire. \n";
  die();
}



my $dico;

if($config_fileDico) {
  if($config_verbose) {
    print STDERR "Chargement du lexique... ";
  }

  my $nbWordsInLex = 0;
  open(my $fh, '<:utf8', $config_fileDico) or die($!);
  while(<$fh>) {
    my $line = $_;
    if($line=~m/^(.+?)($|\t|\n)/) {
      my $form = $1;
      push(@{$dico->{$form}}, 1);
      ++$nbWordsInLex;
    }
  }
  close($fh);

  if($config_verbose) {
    print STDERR "Terminé.\n";
  }
}



my $pass1;

if($config_pass1) {  # On a effectué une première passe (uniq -c sur la liste des tokens)
  open(my $fh, '<:utf8', $config_pass1) or die($!);
  while(<$fh>) {
    if($_=~m/([0-9]+)\s+(.+?)\t/) {
      $pass1->{$2} = $1;
    }
  }
  close($fh);
}



my @prefixes = ();
if($config_fallbackSuperseg) {
  open(my $fh, '<:utf8', $config_fallbackSuperseg) or die($!);
  while(<$fh>) {
    my $line = $_;
    chomp($line);
    if($line!~m/^\s*#/ && $line!~m/^\s*$/) {
      push(@prefixes, $line);
    }
  }
  close($fh);
}

my @sufixes = ();
if($config_fallbackSupersegSufix) {
  open(my $fh, '<:utf8', $config_fallbackSupersegSufix) or die($!);
  while(<$fh>) {
    my $line = $_;
    chomp($line);
    if($line!~m/^\s*#/ && $line!~m/^\s*$/) {
      push(@sufixes, $line);
    }
  }
  close($fh);
}



my $unk;

while (my $stream = <STDIN>) {  # Normalement, un stream = une ligne. Attention, aucun token ne doit être sur plusieurs lignes (on ne recolle pas les lignes).
  chomp($stream);
  
  $stream=~s/([ ,;:\.'\?!()'"\-«»\(\)\[\]])/\n$1\n/gs;  # Séparateurs qu'on conserve
  $stream=~s/^((「[^」]+?」\s*)+)/\n$1\n/gm;  # Décaller les balises en début de token
  $stream=~s/((「[^」]+?」\s*)+)$/\n$1\n/gm;  # Décaller les balises en fin de token
  $stream=~s/ +/ /gs;  # Supprimer espaces doubles
  $stream=~s/\n+/\n/gs;  # Supprimer lignes vides
  
  # Nettoyage "lourd" des erreurs d'OCR
  if($config_ocr=~m/Rmhyphens1/) {
    $stream=~s/\s+-\s+[*1;,]\s+//gi;
    $stream=~s/([a-zàâäæéèêëîïôöœùûüÿç])-[1*;,�'"](\s)/$1$2/gi;
    $stream=~s/([a-zàâäæéèêëîïôöœùûüÿç])[1*�•](\s)/$1$2/gi;
    $stream=~s/<\/p>\s*<p[^>]*>\s*[^A-ZÀÂÄÆÉÈÊËÎÏÔÖŒÙÛÜŸÇ]//g;  # Un § qui ne commence pas par une majuscule ? Impossible !
  }
  
  $stream=~s/<(.+?)>/'<'.rmN($1).'>'/egs;  # Recoller le contenu des balises
  
  my @tokens = split("\n", $stream);
  
  # Resoudage ciblé des tokens en fonction du lexique  
  # Attention, une espace = un token
  if($config_fileDico) {
    my $nbFusions = 0;
    my @newTokens = ();
    for(my $i=0; $i<@tokens; ++$i) {  # Pour chaque token
      for(my $j=$config_window; $j>0; --$j) {  # Taille de la fenêtre à tester
        my $concat = $tokens[$i];
        for(my $k=1; $k<$j; ++$k) {
          if($i + $k < @tokens) {
            $concat .= $tokens[$i + $k];
          }
        }
        my $concatNorm = $concat; $concatNorm=~s/「[^」]+?」//g; $concatNorm=~s/\*//g;
        if( $j==1 || $dico->{$concat} || $dico->{lc($concat)} || $dico->{lc($concatNorm)} ) {  # Si token dans le dico ou bien impossible d'y trouver un token plus petit (j==1)
          if( $config_unk && $concat!~m/^\s+$/ && $concat!~m/「/ && $concat!~m/^</ && ! ($dico->{$concat} || $dico->{lc($concat)} || $dico->{lc($concatNorm)}) ) {  # Pour les tokens qui ne sont pas des espaces ni des balises ni des nombres et ne sont pas dans le dictionnaire
                      
            # Erreurs d'OCR systématiques
            if($config_ocr=~m/ies_les/ && $concat eq 'ies') {
              $concat = "les\tCORRIGÉ";
            }
            elsif($config_ocr=~m/ee_de/ && $concat eq 'ee') {
              $concat = "de\tCORRIGÉ";
            }
            
            # Bien formé
            elsif( $concat=~m/^[A-ZÀÂÄÆÉÈÊËÎÏÔÖŒÙÛÜŸÇ]*[a-zàâäæéèêëîïôöœùûüÿç]*$/ && $concat!~m/(.)\1\1/ && $concat!~m/^(.)\1/) {
                          
              my $threshold = 1;
              if($unk->{$concat}++ > $threshold || ($pass1->{$concat} && $pass1->{$concat} > $threshold)) {  # Un mot inconnu, mais bien formé et qu'on retrouve plusieurs fois
                $concat .= "\tUNK_-1";
              }
              else {
                $concat .= "\tUNK_0";
              }
            }
            
            # Trois fois la même lettre... essayer avec deux
            elsif($concat=~m/(.*)(.)\2\2(.*)/ && $dico->{$concat=~s/(.*)(.)\2\2(.*)/$1$2$2$3/r} || $dico->{lc($concat=~s/(.*)(.)\2\2(.*)/$1$2$2$3/r)}) {
              $concat = ($concat=~s/(.*)(.)\2\2(.*)/$1$2$2$3/r) . "\tCORRIGÉ";
            }
            
            # Mal formé, introuvable
            else {  
              $concat .= "\tUNK_1";
            }
            
          }
          push(@newTokens, $concat);
          if($j>1) {
            ++$nbFusions;
          }
          $i+=($j-1);
          last;
        }
      }
    }
    @tokens = @newTokens;  # Tokens de la ligne
  }
  

  # Essayer de recoller les tokens séparés par un hyphen
  # Exemple:
  #  assu	UNK_-1
  #  -
  #
  #  jettissait	UNK_0
  if($config_fallbackRmhyphens) {
    my @seps = '-';  # Normalement, on ne teste le recollage que sur les hyphens
    my @repl = '';   # Caractère par lequel remplacer le caractère sur lequel on colle
    
    if($config_ocr=~m/Rmhyphens1/) {
      push(@seps, ('Ç', '!', "'", "'", '"', '"', '"', '.', '�', '«', '»'));
      push(@repl, ('C', 'l', 'l', '',  '',  'u', 'c', '',  '',  '',  ''));
    }
    if($config_fallbackMergeseg) {
      push(@seps, (' '));
      push(@repl, (''));
    }
    
    for(my $s=0; $s<@seps; ++$s) {
      my $sep = $seps[$s];
      my $rep = $repl[$s];
      for(my $i=1; $i<@tokens; ++$i) {
        if($tokens[$i] eq $sep || $tokens[$i]=~s/\t.*//r eq $sep) {
          if($i+1<@tokens && $tokens[$i-1]!~m/^\s*$/ && $tokens[$i+1]!~m/^\s*$/ && ($tokens[$i-1]=~m/\tUNK_/ || $tokens[$i+1]=~m/\tUNK_/) ) {  # Cas 1: sans espace après l'hyphen
            my $tokA = $tokens[$i-1]=~s/\t.*//r;
            my $tokB = $tokens[$i+1]=~s/\t.*//r;
            my $conc = $tokA.$rep.$tokB;
            if($dico->{$conc} || $dico->{lc($conc)}) {  # Si la concaténation donne un mot connu
              $tokens[$i-1] = '';
              $tokens[$i] = $conc."\tCORRIGÉ";  # Remplacer l'hyphen par la concaténation des deux tokens
              $tokens[$i+1] = '';
            }
          }
          elsif($i+2<@tokens && $tokens[$i-1]!~m/^\s*$/ && $tokens[$i+1]=~m/^\s*$/ && $tokens[$i+2]!~m/^\s*$/ && ($tokens[$i-1]=~m/\tUNK_/ || $tokens[$i+2]=~m/\tUNK_/) ) {  # Cas 2: avec espace après l'hyphen
            my $tokA = $tokens[$i-1]=~s/\t.*//r;
            my $tokB = $tokens[$i+2]=~s/\t.*//r;
            my $conc = $tokA.$rep.$tokB;
            if($dico->{$conc} || $dico->{lc($conc)}) {  # Si la concaténation donne un mot connu
              $tokens[$i-1] = '';
              $tokens[$i] = $conc."\tCORRIGÉ";  # Remplacer l'hyphen par la concaténation des deux tokens
              $tokens[$i+2] = '';
            }
          }
        }
      }
    }
  }

  
  # Essayer de sursegmenter les tokens inconnus (préfixes)
  if($config_fallbackSuperseg) {
    foreach my $prefix (@prefixes) {  # Pour chaque préfixe
      for(my $i=0; $i<@tokens; ++$i) {  # Pour chaque token
        my $token = $tokens[$i]=~s/\t.*//r;
        if($tokens[$i]=~m/\tUNK_/ && $token=~m/^($prefix)(.+)$/i) {  # Token inconnu et commence par un préfixe
          my $tokenA = $1;
          my $tokenB = $2;
          if($dico->{$tokenB} || $dico->{lc($tokenB)}) {
            $tokens[$i] = "$tokenA\tCORRIGÉ\n$tokenB\tCORRIGÉ";  # ATTENTION, ça ne marche que parce que on fait un join sur @tokens juste après !
          }
        }
      }
    }
  } 
  
  # Essayer de sursegmenter les tokens inconnus (suffixes)
  if($config_fallbackSupersegSufix) {  
    foreach my $sufix (@sufixes) {  # Pour chaque préfixe
      for(my $i=0; $i<@tokens; ++$i) {  # Pour chaque token
        my $token = $tokens[$i]=~s/\t.*//r;
        if($tokens[$i]=~m/\tUNK_/ && $token=~m/^(.+)($sufix)$/i) {  # Token inconnu et commence par un préfixe
          my $tokenA = $1;
          my $tokenB = $2;
          if($dico->{$tokenA} || $dico->{lc($tokenA)}) {
            $tokens[$i] = "$tokenA\tCORRIGÉ\n$tokenB\tCORRIGÉ";  # ATTENTION, ça ne marche que parce que on fait un join sur @tokens juste après !
          }
        }
      }
    }
  } 
  
  # Essayer de sursegmenter les tokens inconnus (NP)
  if($config_fallbackSupersegNP) {  
    for(my $i=0; $i<@tokens; ++$i) {  # Pour chaque token
      my $token = $tokens[$i]=~s/\t.*//r;
      if($tokens[$i]=~m/\tUNK_/ && $token=~m/^([A-Za-z]+)([A-Z][a-z]+)$/) {  # xxxxxYyyyy
        my $tokenA = $1;
        my $tokenB = $2;
        if(($dico->{$tokenA} || $dico->{lc($tokenA)}) && ($dico->{$tokenB} || $dico->{lc($tokenB)})) {
          $tokens[$i] = "$tokenA\tCORRIGÉ\n$tokenB\tCORRIGÉ";  # ATTENTION, ça ne marche que parce que on fait un join sur @tokens juste après !
        }
      }
    }
  } 


  
  $stream = join("\n", @tokens);
  $stream=~s/^((「[^」]+?」\s*)+)/\n$1\n/gm;  # Décaller les balises en début de token
  $stream=~s/((「[^」]+?」\s*)+)$/\n$1\n/gm;  # Décaller les balises en fin de token
  $stream=~s/ +/ /gs;  # Supprimer espaces doubles
  $stream=~s/\n+/\n/gs;  # Supprimer lignes vides  

  chomp($stream);
    
  if($config_nbTokens) {
    print split(/\n/, $stream) + 0;
    print "\t" . ($stream=~s/\n/\t/rg);
  }
  else {
    print "$stream";
  }
  print "\n";

 
  
}


sub rmN {
  my $str = shift;
  $str=~s/\n//gs;
  return $str;
}
