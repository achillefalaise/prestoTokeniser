<html>
  <head>
    <style>
      form div {
        margin: 10px;
      }
    </style>
  </head>
  <body>
    <form method="post">
    
      <div>
      Sélectionner un jeu de données:<br/>
        <select name="dataset" onchange="this.form.submit()">
          <option value=""></option>
          <?php
            foreach(scandir('.') as $dir)
              if(is_dir($dir) && ! preg_match('/^\./', $dir))
                print "<option value=\"$dir\" ".($_REQUEST['dataset']==$dir?'selected':'').">$dir</option>";
          ?>
        </select>
      </div>
      
      <?
        if($_REQUEST['dataset']) {
      ?>
        <div>
          Lexique: <a href="<?='./'.$_REQUEST['dataset'].'/lexicon.csv'?>">lexicon.csv</a><br/>
          Préfixes: <a href="<?='./'.$_REQUEST['dataset'].'/superseg-prefix.txt'?>">superseg-prefix.txt</a><br/>
          Suffixes: <a href="<?='./'.$_REQUEST['dataset'].'/superseg-sufix.txt'?>">superseg-prefix.txt</a><br/>
        </div>
        <div>
          Texte à annoter:<br/>
          <textarea name="text" cols="80" rows="20"><?=$_REQUEST['text']?></textarea>
        </div>
      <?
        }
      ?>
    
      <?
        if($_REQUEST['dataset']) {
      ?>  
      <div>
        <input type="submit"/>
      </div>
      <?
        }
      ?>
           
    </form>
    
    <form>   
      <?
        if($_REQUEST['dataset'] && $_REQUEST['text']) {
          $fileHandle = tmpfile();
          $filePath = stream_get_meta_data($fileHandle)['uri'];
          fwrite($fileHandle, $_REQUEST['text']);
      ?>  
      <hr/>
      <div>
          Normalisation:<br/>
          <textarea name="text" cols="80" rows="20"><?=passthru('cat "'.$filePath.'" | perl normalise.pl | grep -vP "^\s*$"')?></textarea>
      </div>
      <div>
          Tokenisation:<br/>
          <textarea name="text" cols="80" rows="20"><?=passthru('cat "'.$filePath.'" | perl normalise.pl | perl tokenise.pl -l prestoRessources/lexicon.csv -w 5 -p prestoRessources/superseg-prefix.txt -s prestoRessources/superseg-sufix.txt | grep -vP "^\s*$"')?></textarea>
      </div>
      <?
          fclose($fileHandle); // ceci va effacer le fichier  
        }
      ?>
    </form>
    
  </body>
</html>
