# Découper en tokens

Ce projet a pour but de convertir un _texte_ en une séquence de _tokens_, en fonction d'un _lexique_ (une liste de tokens). Il est développé pour des documents XML en français classique (XVIIe-XVIIIe siècles) et pré-classique (XVIe siècle).

Ce projet comporte trois scripts: 
* un normaliseur de textes (script _normaliser.pl_),
* un script de tokenisation (script _tokeniser.pl_),
* une interface Web optionnelle (script _index.php_).

Les tokens du lexique peuvent comporter plusieurs mots typographiques ("aujourd'hui", "c'est-à-dire", "par conséquent"...). Deux listes permettent de traiter les cas où un mot typographique correspond à deux tokens (formes anciennes "tresgrand", "luymesme" par exemple).

Le texte en entrée peut comporter du XML (la sortie sera alors: une balise = un token).

## Motivation
De nombreux taggers (notamment TreeTagger) ne savent pas analyser directement un texte, mais travaillent sur des _tokens_ préalablement délimités. Par exemple, TreeTagger prend en entrée un fichier texte contenant un _token_ par ligne. Un _token_ correspond souvent à un _mot_, c'est à dire une séquence de caractères alphabétiques délimités par des séparateurs typographiques (espaces, ponctuation)... mais pas toujours. Cela dépend en fait totalement du lexique utilisé par le tagger.

Ainsi, la plupart des lexiques du français moderne voient deux mots dans "l'arbre" ("l'" et "arbre"), mais un seul token dans ("aujourd'hui"); par conséquent, l'apostrophe ne peut pas toujours être considérée comme un séparateur de tokens.

Beaucoup de lexiques utilisés en TAL comportent aussi des expressions multi-mots, par exemple la locution adverbiale "par conséquent" (deux mots typographiques, mais un seul token, de catégorie adverbe).

Par ailleurs, en français classique (XVIIe-XVIIIe siècles), l'adverbe "très" est souvent concaténé avec l'adjectif qu'il précède ("trèsbon", "trèsgrand"). L'adjectif "même" ("mesme" en orthographe classique) est souvent concaténé avec le pronom qu'il suit ("luymesme", "soymesme").

Enfin, dans beaucoup de systèmes d'écriture (chinois, coréen, japonais, vietnamien, mais aussi graphies anciennes des langues occidentales...), il n'y a pas de séparateur de mots, et la notion de _token_ (délimité par des séparateurs) n'y est pas pertinente.

## Algorithme
L'algorithme utilisé ici est celui dit "la plus longue chaîne possible". Il aurait été décrit pour la première fois dans « Liang, N. Y. (1986) "On computer automatic word segmentation of written Chinese". _Journal of Chinese Information Processing 1 (1)_ », mais cet article, rédigé en chinois, n'a apparemment jamais été numérisé et est introuvable.

Cet algorithme donne de bons résultats en chinois, où le nombre d'ambiguïtés de tokenisation est très élevé. Il est ici adapté pour le français moderne et classique, mais devrait pouvoir être utilisé avec tous les systèmes d'écriture comportant des séparateurs typographiques.

L'algorithme comporte deux étapes:
1. Découpage "maximal" du texte, en se basant sur la typographie. En chinois, cela revient à faire un découpage par caractères. Dans cette adaptation pour le français, le découpage se fait sur les séparateurs typographiques (espaces, ponctuation), et deux listes servent à traiter les préfixes/suffixes concaténés avec un mot ("tresgrand", "luymesme").
2. Fusion des tokens, en fonction du lexique, afin d'obtenir la plus longue chaîne possible.

Par exemple "aujourd'hui" est découpé lors de la première étape (il comporte un séparateur typographique), puis recollé lors de la seconde (le lexique contient une entrée "aujourd'hui").

## Utilisation
Le script prend en paramètre un lexique (paramètre -l) comportant un token par ligne. Le lexique peut être au format texte (un token par ligne), ou bien CSV (colonnes séparées par des tabulations); dans ce cas, seule la première colonne est utilisée. Le lexique n'a pas besoin de contenir les tokens mono-mots. Si aucun lexique n'est fourni, la tokenisation se fait uniquement en fonction des caractères (découpage maximal décrit à l'étape 1 de l'algorithme).

> echo "Le monde d'aujourd'hui." | perl tokenise.pl -l sampleResources/lexiconSample.csv -w 3

### Paramètre optionnel
* *-l _path_* Emplacement du lexique. Un token par ligne.
* *-p _path_* Emplacement d'une liste de préfixes à découper (par exemple pour "tres" pour "tresgrand", "tresbon"...). Un préfixe par ligne.
* *-s _path_* Emplacement d'une liste de suffixes à découper (par exemple pour "mesme" pour "soymesme", "luymesme"...). Un suffixe par ligne.
* *-v* Active le mode verbeux.
* *-w _n_* Taille max d'un token en mots, y compris les séparateurs ("aujourd'hui" fait 3 tokens: "aujourd", "'" et "hui"; "parce que" fait aussi 3 tokens: "parce", " " et "que"). Valur par défaut: 5.

Le texte à traiter est passé par un pipe. Pour normaliser puis tokéniser un fichier:

`cat monfichier.txt | perl normalise.pl | perl tokenise.pl`

ou

`cat monfichier.txt | perl normalise.pl  | perl tokenise.pl -l sampleResources/lexicon.csv -w 5 -p sampleResources/superseg-prefix.txt -s sampleResources/superseg-sufix.txt -`

La liste des séparateurs est codée en dur dans le code. Il s'agit de [ ,;:\.'\?!()'"\-«»\(\)\[\]]. Pour la modifier, chercher le texte "Séparateurs" dans le code source.

Ce script ne traite que de l'UTF-8.

## Lignes vides

Le script a tendance à créer des lignes vides; on peut les enlever avec un grep:

`cat monfichier.txt | perl normalise.pl | perl tokenise.pl | grep -vP "^\s*$"`

## Installation
Ce script en _Perl_ requiert les dépendances suivantes:
* cpan -i File::Slurp
* cpan -i Getopt::Long
* cpan -i Switch
* cpan -i Try::Tiny
* cpan -i XML::Entities

## Crédits
Développement: Achille Falaise

Financement:
* ANR (2014-2016) puis labex ASLAN (2017, 2021-2022) et CNRS (2021-2022)

Cadre:
* projet Presto, laboratoire ICAR (2014-2017)
* projet Disco, laboratoire LLF (2021-2022)

## Licence
Licence GPL v3
