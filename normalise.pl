#!/usr/bin/perl -w

# Installation:
#  sudo cpan -i XML::Entities

use XML::Entities;

# Normalisation générique des textes pour tokenise.pl.

use strict;
use warnings;

# Tout passer en UTF-8
use v5.14;
use utf8;
binmode STDOUT, ':utf8';
binmode STDIN, ":utf8";  
use open qw(:std :utf8);
use Encode qw(decode_utf8);

# Charger le fichier. (Attention, on charge tout le fichier ! Travailler sur des fichiers de taille raisonnable.)
my $fileContent = '';
my $nbLines = 0;
while(my $line = <STDIN>) {
  $fileContent .= $line;
  ++$nbLines;
}

# Hyphens
$fileContent=~s/(\p{Letter})[¬-]\s*[\n\r]/$1/g;

# Blancs
$fileContent=~s/\0/ /g;  # Remplacer les caractères null par des espaces
$fileContent=~s/\t/ /g;  # Remplacer les tabulations par des espaces
$fileContent=~s/[\n\r]+/ /g;  # Enlever les retours chariot

# Typos
$fileContent=~s/’/'/g;  # Apostrophes
$fileContent=~s/([0-9]+),([0-9]+)/$1$2/g;  # Nombres à virgule

# Entités
$fileContent=~s/&(g|l)t;/~$1t;/g;  # Protéger les < >
$fileContent = XML::Entities::decode('all', $fileContent);
$fileContent=~s/~(g|l)t;/&$1t;/g;  # Restaurer les < >

# Mise en forme
$fileContent=~s/(<[^>]+?>)/\n$1\n/g; # Ajouter des retours chariots sur les balises
$fileContent=~s/\n\s*\n/\n/gs;  # Enlever les lignes vides
$fileContent=~s/^\s*//gm;  # Enlever les espaces en début de ligne

print $fileContent;
